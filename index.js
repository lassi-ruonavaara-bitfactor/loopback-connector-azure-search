'use strict';
/*
 * Copyright (c) 2017 Houston Inc. Consulting Oy
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

const Connectors = require('loopback-connector');
const inherits = require('util').inherits;
const request = require('superagent');
const GeoPoint = require('loopback').GeoPoint;
const debug = require('debug')('loopback:connector:azure-search');

exports.initialize = function(dataSource, callback) {
    const settings = dataSource.settings;

    dataSource.connector = new AzureSearch(settings);

    if (callback) {
        dataSource.connector.connect(callback);
    }
};

function AzureSearch(settings) {
    Connectors.Connector.call(this, 'azuresearch', settings);

    this.host = settings.host;
    this.apiKey = settings.adminOrQueryKey;
    this.allowUnsafeKeys = !!settings.allowNonUrlSafeIDs;

    if (this.host === undefined || this.host === null || this.apiKey === undefined || this.apiKey === null) {
        throw new Error('"host" and "apiKey" are required configuration properties.');
    }

    // Initialize a retry policy. Currently only a constant wait
    // is supported.
    if (settings.retry !== null && settings.retry !== undefined) {
        this.retryPolicy = {
            max: settings.retry.max,
            wait: function(retries) {
                return settings.retry.waitInMilliseconds;
            }
        };
    }
}

inherits(AzureSearch, Connectors.Connector);

AzureSearch.prototype.connect = function(callback) {
    debug('Connecting to %j with retry policy %j', this.host, this.retryPolicy);
    callback(null);
};

AzureSearch.prototype.disconnect = function(callback) {
    debug('Disconnecting from %j', this.host);
    callback(null);
};

AzureSearch.prototype.bulkExecute = function(model, data, options, callback) {
    const modelDefinition = this.getModelDefinition(model);

    return documentWrite(this.host, this.apiKey, this.retryPolicy, this.allowUnsafeKeys, modelDefinition, 0, liftData(modelDefinition, data), callback);
};

AzureSearch.prototype.create = function(model, data, options, callback) {
    options = options || {};

    const modelDefinition = this.getModelDefinition(model);
    const markedAsRemoved = options.markedAsRemoved || [];

    delete options.markedAsRemoved;

    return this.bulkExecute(model, liftData(modelDefinition, data, options.defaultIndexAction || 'upload', markedAsRemoved), options, callback);
};

AzureSearch.prototype.updateOrCreate = function(model, data, options, callback) {
    options = options || {};

    const modelDefinition = this.getModelDefinition(model);
    const markedAsRemoved = options.markedAsRemoved || [];

    delete options.markedAsRemoved;

    return this.bulkExecute(model, liftData(modelDefinition, data, options.defaultIndexAction || 'mergeOrUpload', markedAsRemoved), options, callback);
};

AzureSearch.prototype.replaceOrCreate = AzureSearch.prototype.create;

AzureSearch.prototype.replaceById = function(model, id, data, options, callback) {
    return callback(new Error('Not supported: please use replaceOrCreate().'));
};

AzureSearch.prototype.all = function(model, filter, options, callback) {
    const modelDefinition = this.getModelDefinition(model);

    try {
        const query = buildQueryForModel(modelDefinition, filter);

        return documentSearchWithContinuation(this.host, this.apiKey, this.retryPolicy, modelDefinition, query, liftValueResponse(modelDefinition, callback));
    } catch (error) {
        return callback(error);
    }
};

AzureSearch.prototype.save = function(model, data, options, callback) {
    this.updateOrCreate(model, data, options, function(error, id) {
        if (error) {
            return callback(error);
        }

        data.id = id;
        callback(null, data);
    });
};

AzureSearch.prototype.count = function(model, where, options, callback) {
    const modelDefinition = this.getModelDefinition(model);

    try {
        const query = buildQueryForModel(modelDefinition, { where: where });

        query.count = true; // Count results.
        query.top = 0; // Don't return any documents.

        return documentSearch(this.host, this.apiKey, this.retryPolicy, modelDefinition, 0, query, liftCountResponse(callback));
    } catch (error) {
        return callback(error);
    }
};

AzureSearch.prototype.update = function(model, where, data, options, callback) {
    throw new Error('Not supported: use "updateOrCreate()".');
};

AzureSearch.prototype.destroyAll = function(model, where, options, callback) {
    const modelDefinition = this.getModelDefinition(model);
    const idName = this.idName(model);

    try {
        const query = buildQueryForModel(modelDefinition, {
            where,
            fields: [idName],
        });

        return documentSearchWithContinuation(this.host, this.apiKey, this.retryPolicy, modelDefinition, query, (error, response) => {
            if (error) {
                return callback(error);
            }

            const actions = response.body.value.map(value => ({
                '@search.action': 'delete',
                [idName]: value[idName],
            }));

            return documentWrite(this.host, this.apiKey, this.retryPolicy, this.allowUnsafeKeys, modelDefinition, 0, actions, callback);
        })
    } catch (error) {
        return callback(error);
    }
};

AzureSearch.prototype.updateAttributes = function(model, id, data, options, cb) {
    // Azure Search has no native support for individual attribute updates.
    // Instead we just update documents with given ID and replace them
    // with newer versions.
    this.update(model, { id: id }, data, options, cb);
};

AzureSearch.prototype.autoupdate = function(models, cb) {

    // Apply index updates in chain as long as there
    // are indices left.
    const next = modelDefinitions => {

        // If there are no indices left, finish up autoUpdate()
        // with no errors.
        if (modelDefinitions.length === 0) {
            return cb(null);
        }

        createOrUpdateIndexForModel(this.host, this.apiKey, modelDefinitions[0], function(error) {

            // Stop if we got an error while updating the index.
            if (error) {
                debug('Error while updating index: %j', error);
                return cb(error);
            }

            debug('Created or updated index %j.', indexName(modelDefinitions[0]));

            // Update the next index.
            next(modelDefinitions.slice(1));
        });
    };

    // Apply index update for all models.
    next(models.map(x => this.getModelDefinition(x)));
};

AzureSearch.prototype.automigrate = function(models, cb) {

    // Remove indices in chain as long as there are
    // indices left.
    const next = modelDefinitions => {

        // If there are no indices left, apply autoUpdate() operation to re-create them.
        if (modelDefinitions.length === 0) {
            return this.autoupdate(models, cb);
        }

        removeIndexForModel(this.host, this.apiKey, modelDefinitions[0], function(error) {
            if (error) {
                debug('Cannot remove %j, skipping...', indexName(modelDefinitions[0]));
            } else {
                debug('Removed index %j.', indexName(modelDefinitions[0]));
            }

            // Remove the next index.
            next(modelDefinitions.slice(1));
        });
    };

    // Remove all model indices.
    next(models.map(x => this.getModelDefinition(x)));
};

function documentWrite(host, apiKey, retryPolicy, allowUnsafeKeys, modelDefinition, retryCount, data, cb) {
    const failures = [];

    // We respect the REST API item limit by updating every document in 1000 item
    // chunks. Also, because Azure Search does not support batch transactions
    // with chunks, we ignore individual chunk errors while writing chunks and
    // report those later to the caller. A caller can then decide what to do with the
    // failed chunks. Callers may want to implement their own retry policy and try
    // to write the failed chunks again to the search index.
    const next = data => {

        // End when the data runs out.
        if (data.length === 0) {

            // Check if there are any failures and report them to caller.
            if (failures.length > 0) {
                const error = new Error('There was an error while batch writing documents. Please see \'failures\' property for more details.');

                error.failures = failures;

                // Retry automatically if retry policy is given and there are only errors that can be retried.
                if (retryPolicy !== undefined && retryCount < retryPolicy.max && !failures.some(x => !isRetriable(x.error))) {
                    const flatChunks = failures.map(x => x.data).reduce((x, acc) => x.concat(acc));

                    debug('Write to %j failed, will try again in %jms (%j of total %j retries).', indexName(modelDefinition), retryPolicy.wait(retryCount), retryCount + 1, retryPolicy.max);

                    return setTimeout(function() {
                        documentWrite(host, apiKey, retryPolicy, allowUnsafeKeys, modelDefinition, retryCount + 1, flatChunks, cb);
                    }, retryPolicy.wait(retryCount));
                }

                return cb(error);
            }

            // Succeeded without failures.
            return cb(null);
        }

        const chunk = data.slice(0, 1000);

        apiPost([host, 'indexes', indexName(modelDefinition), 'docs', 'index'], apiKey, allowUnsafeKeys)
            .send({ value: chunk })
            .set('accept', 'json')
            .end(function(error) {

                // If there was an error, save the failure state and
                // report all failures to caller later.
                if (error) {
                    failures.push({ data: chunk, error: error });
                    debug('Failed to write %j of %j document(s) to %j.', chunk.length, data.length, indexName(modelDefinition));
                } else {
                    debug('Wrote %j of %j document(s) to %j.', chunk.length, data.length, indexName(modelDefinition));
                }

                // Ignore errors at this point and just continue to the next chunk.
                next(data.slice(1000));
            });
    };

    return next(data);
}

function documentSearchWithContinuation(host, apiKey, retryPolicy, modelDefinition, query, cb) {
    const topQuery = Object.assign({}, { top: 1001 }, query);

    return documentSearch(host, apiKey, retryPolicy, modelDefinition, 0, topQuery, function(error, response) {
        if (error) {
            return cb(error);
        }

        const next = (response, expectedCount) => {
            // Stop continuation if there are no pages left or if we got the expected
            // amount of documents.
            if (response.body['@search.nextPageParameters'] === undefined || expectedCount !== undefined && response.body.value.length >= expectedCount) {

                // Inject count to document level so that it gets mapped correctly to properties in view.
                for (let i = 0; i < response.body.value.length && response.body['@odata.count'] !== undefined; ++i) {
                    response.body.value[i]['@odata.count'] = response.body['@odata.count'];
                }

                return cb(null, response);
            }

            // Because REST API's document upper limit is 1000 documents,
            // we force it to return all it can plus one in every request
            // when caller hasn't specified their own number.
            if (expectedCount === undefined) {
                response.body['@search.nextPageParameters'].top = 1001;
            }

            // Delete queryType OData type annotation because it is not supported.
            delete response.body['@search.nextPageParameters']['queryType@odata.type'];

            // Make a next page search.
            documentSearch(host, apiKey, retryPolicy, modelDefinition, 0, response.body['@search.nextPageParameters'], function(error, nextResponse) {

                // Stop if we got an error in any of the pages. Do not return partial results.
                if (error) {
                    return cb(error);
                }

                // Join previous results in front of the next results.
                nextResponse.body.value = response.body.value.concat(nextResponse.body.value);

                // Sum result counts if defined.
                if (nextResponse.body['@odata.count'] !== undefined && response.body['@odata.count'] !== undefined) {
                    nextResponse.body['@odata.count'] += response.body['@odata.count'];
                }

                // Go to next page.
                next(nextResponse, expectedCount);
            });
        };

        next(response, query.top);
    });
}

function documentSearch(host, apiKey, retryPolicy, modelDefinition, retryCount, query, cb) {
    debug('Query from %j: %j', indexName(modelDefinition), query);

    return apiPost([host, 'indexes', indexName(modelDefinition), 'docs', 'search'], apiKey)
            .send(query)
            .set('accept', 'json')
            .end(function(error, results) {
                if (error && retryCount < retryPolicy.max && isRetriable(error)) {
                    debug('Query from %j failed, will try again in %jms (%j of total %j retries).', indexName(modelDefinition), retryPolicy.wait(retryCount), retryCount + 1, retryPolicy.max);

                    return setTimeout(function() {
                        documentSearch(host, apiKey, retryPolicy, modelDefinition, retryCount + 1, query, cb);
                    }, retryPolicy.wait(retryCount));
                }

                return cb(error, results);
            });
}

function liftValueResponse(modelDefinition, cb) {
    return function(error, response) {
        if (error) {
            return cb(liftError(error));
        }

        cb(null, response.body.value.map(x => liftViewProperties(modelDefinition, x)));
    };
}

function liftCountResponse(cb) {
    return function(error, response) {
        if (error) {
            return cb(liftError(error));
        }

        cb(null, response.body['@odata.count']);
    };
}

function liftError(error) {
    if (error && error.response && error.response.text) {
        return error.response.text;
    }

    return error;
}

function isRetriable(error) {
    return error && error.response && error.response.status !== 400;
}

function buildQueryForModel(modelDefinition, filter) {
    filter = filter || {};

    const query = {

        // Set full query mode to allow Lucene syntax.
        queryType: 'full'
    };

    if (filter.fields !== undefined) {
        query.select = buildSelectForModel(modelDefinition, filter.fields);
    }

    if (filter.searchFields !== undefined) {
        query.searchFields = buildSearchFieldsForModel(modelDefinition, filter.searchFields);
    }

    if (filter.order !== undefined) {
        query.orderby = buildOrderByForModel(modelDefinition, filter.order);
    }

    if (filter.where !== undefined) {
        query.search = buildSearchQueryForModel(filter.where);
        query.filter = buildFilterQueryForModel(modelDefinition, filter.where);
    }

    if (filter.offset !== undefined) {
        query.skip = filter.offset;
    }

    if (filter.limit !== undefined) {
        query.top = filter.limit;
    }

    // Treat this as a pagination request, so apply implicit count.
    if (filter.offset !== undefined && filter.limit !== undefined) {
        query.count = true;
    }

    if (filter.include !== undefined) {
        throw new Error('Not supported filter: Include filter is not supported.');
    }

    return query;
}

function buildFilterQueryForModel(modelDefinition, where, variables) {
    return Object.keys(where).map(x => {
        const normalizedKey = x.toLowerCase().trim();

        // Build a top-level logical operator.
        if (['and', 'or'].indexOf(normalizedKey) >= 0) {
            const logicalClause = where[x]
                .map(y => buildFilterQueryForModel(modelDefinition, y))
                .filter(y => y !== undefined)
                .join(` ${normalizedKey} `);

            if (logicalClause.length > 0) {
                return `(${logicalClause})`;
            }

            // No logical clause.
            return undefined;
        }

        const localVariables = ['*'].concat(variables);
        const dbProperty = translateDBPropertyFromView(modelDefinition, x, localVariables);
        let property = modelDefinition.properties[x];

        if (localVariables.indexOf(dbProperty) >= 0) {
            property = { type: String };
        }

        // Handle null value filtering.
        if (where[x] === null) {
            return `${dbProperty} eq ${escapeFieldForQuery(property, null)}`;
        }

        // Build greater than operator.
        if (where[x]['gt'] !== undefined) {
            return `${dbProperty} gt ${escapeFieldForQuery(property, where[x]['gt'])}`;
        }

        // Build greater than or equal to operator.
        if (where[x]['gte'] !== undefined) {
            return `${dbProperty} ge ${escapeFieldForQuery(property, where[x]['gte'])}`;
        }

        // Build less than operator.
        if (where[x]['lt'] !== undefined) {
            return `${dbProperty} lt ${escapeFieldForQuery(property, where[x]['lt'])}`;
        }

        // Build less than or equal to operator.
        if (where[x]['lte'] !== undefined) {
            return `${dbProperty} le ${escapeFieldForQuery(property, where[x]['lte'])}`;
        }

        // Build non-equality operator.
        if (where[x]['neq'] !== undefined) {
            return `${dbProperty} ne ${escapeFieldForQuery(property, where[x]['neq'])}`;
        }

        // Build between operator with greater than or equal to and less than or equal to operators.
        if (where[x]['between'] !== undefined) {
            return `(${dbProperty} ge ${escapeFieldForQuery(property, where[x]['between'][0])} and ${dbProperty} le ${escapeFieldForQuery(property, where[x]['between'][1])})`;
        }

        // Build in operator with search.in function. | is used as a value separator operator.
        if (where[x]['inq'] !== undefined) {
            const params = where[x]['inq'].map(x => convertValue(property, x).replace(/\|/g, '\\|'));

            return `search.in(${dbProperty}, '${params.join('|')}', '|')`;
        }

        // Build not in operator with search.in function. | is used as a value separator operator.
        if (where[x]['nin'] !== undefined) {
            const params = where[x]['nin'].map(x => convertValue(property, x).replace(/\|/g, '\\|'));

            return `not search.in(${dbProperty}, '${params.join('|')}', '|')`;
        }

        // We support only case-insensitive likes.
        if (where[x]['like'] !== undefined) {
            throw new Error('Only case-insensetive like is supported. Please use "ilike".');
        }

        // We support only case-insensitive likes.
        if (where[x]['nlike'] !== undefined) {
            throw new Error('Only case-insensetive nlike is supported. Please use "nilike".');
        }

        // Build case-insensitive like operator.
        if (where[x]['ilike'] !== undefined) {
            return `search.ismatch(${escapeFieldForQuery(property, where[x]['ilike'])}, ${toStringConstant(dbProperty)})`;
        }

        // Build case-insensitive not like operator.
        if (where[x]['nilike'] !== undefined) {
            return `not search.ismatch(${escapeFieldForQuery(property, where[x]['nilike'])}, ${toStringConstant(dbProperty)})`;
        }

        // Parse dynamic operators.
        for (const k in where[x]) {
            const normalizedKey = k.toLowerCase().trim();

            // Build all and any operators with predicate variable binding.
            if (normalizedKey.indexOf('all ') === 0 || normalizedKey.indexOf('any ') === 0) {
                const [operator, variable] = normalizedKey.split(' ', 2);
                const clause = buildFilterQueryForModel(modelDefinition, where[x][k], (variables || []).concat([variable]));

                return `${dbProperty}/${operator}(${variable}: ${clause || ''})`;
            }
        }

        // This is handeld by search query.
        if (x === '*') {
            return undefined;
        }

        // By default, assume equality operator.
        return `${dbProperty} eq ${escapeFieldForQuery(property, where[x])}`;
    }).filter(x => x !== undefined)

      // All clauses are AND if nothing else is specified.
      .join(' and ');
}

function buildSearchQueryForModel(where) {
    if (where['*'] !== undefined) {
        return where['*'].replace(/"/g, '\"');
    }

    return undefined;
}

function buildSelectForModel(modelDefinition, fields) {
    return fields
        // Filter out map and non-retrievable properties.
        .filter(x => !isMapProperty(modelDefinition.properties[x]) && isRetrievableProperty(modelDefinition.properties[x]))
        .map(x => translateDBPropertyFromView(modelDefinition, x))
        .join(',');
}

function buildSearchFieldsForModel(modelDefinition, fields) {
    let include = Object.keys(fields || {}).filter(x => fields[x] === true);

    // By default, every field is included.
    if (include.length === 0) {
        include = properties(modelDefinition);
    }

    return include
        // Filter out non-searchable and explicitly excluded properties.
        .filter(x => isSearchableProperty(modelDefinition.properties[x]) && fields[x] !== false)
        .map(x => translateDBPropertyFromView(modelDefinition, x))
        .join(',');
}

function buildOrderByForModel(modelDefinition, order) {
    return (Array.isArray(order) ? order : [order]).map(x => {
        const parts = x.split(' ', 2);
        const dbProperty = translateDBPropertyFromView(modelDefinition, parts[0]);

        if (parts.length > 1) {
            const type = parts[1].trim();
            const normalizedType = type.toLowerCase();

            if (['asc', 'desc'].indexOf(normalizedType) < 0) {
                throw new Error(`Unsupported order by type '${type}'. Supported types are ASC and DESC.`);
            }

            return `${dbProperty} ${normalizedType}`;
        }

        return dbProperty;
    }).join(',');
}

function toStringConstant(value) {
    return `'${value.toString().replace(/'/g, "\'")}'`;
}

function escapeFieldForQuery(modelProperty, value) {
    // Use null for undefined and null values.
    if (value === undefined || value === null) {
        return 'null';
    }

    const typedValue = convertValue(modelProperty, value);

    // Escape string property.
    if (modelProperty.type === String) {
        return `'${typedValue.replace(/'/g, '\\\'')}'`;
    }

    // Make sure that dates are in ISO format.
    if (modelProperty.type === Date) {
        return typedValue.toISOString();
    }

    // By default, assume any other type without escaping.
    return typedValue.toString();
}

function createOrUpdateIndexForModel(host, apiKey, modelDefinition, cb) {
    const fields = properties(modelDefinition).map(x => {
        const specialSettings = modelDefinition.properties[x].azureSearch || {};

        const field = {
            name: translateDBPropertyFromView(modelDefinition, x),
            type: convertTypeTo(modelDefinition.model.definition.name, x, modelDefinition.properties[x].type, specialSettings)
        };

        if (modelDefinition.properties[x].id !== undefined) {
            field.key = modelDefinition.properties[x].id;
        }

        if (specialSettings.searchable !== undefined) {
            field.searchable = specialSettings.searchable;
        }

        if (specialSettings.filterable !== undefined) {
            field.filterable = specialSettings.filterable;
        }

        if (specialSettings.sortable !== undefined) {
            field.sortable = specialSettings.sortable;
        }

        if (specialSettings.facetable !== undefined) {
            field.facetable = specialSettings.facetable;
        }

        if (specialSettings.retrievable !== undefined) {
            field.retrievable = specialSettings.retrievable;
        }

        if (specialSettings.analyzer !== undefined) {
            field.analyzer = specialSettings.analyzer;
        }

        if (specialSettings.searchAnalyzer !== undefined) {
            field.searchAnalyzer = specialSettings.searchAnalyzer;
        }

        if (specialSettings.indexAnalyzer !== undefined) {
            field.indexAnalyzer = specialSettings.indexAnalyzer;
        }

        return field;
    });

    const definition = {
        fields: fields
    };

    const specialSettings = modelDefinition.settings.azureSearch || {};

    if (specialSettings.analyzers !== undefined) {
        definition.analyzers = specialSettings.analyzers;
    }

    return apiPut([host, 'indexes', indexName(modelDefinition)], apiKey)
        .send(definition)
        .set('accept', 'json')
        .end(cb);
}

function removeIndexForModel(host, apiKey, modelDefinition, cb) {
    return apiDelete([host, 'indexes', indexName(modelDefinition)], apiKey)
        .end(cb);
}

function indexName(modelDefinition) {
    return modelDefinition.settings !== undefined && modelDefinition.settings.azureSearch !== undefined && modelDefinition.settings.azureSearch.indexName !== undefined

        // Use overriden index name.
        ? modelDefinition.settings.azureSearch.indexName

        // Use model name as index name. Note that Azure Search
        // index names are quite limited. Index name must only contain
        // lowercase letters, digits or dashes, cannot start or end with dashes
        // and is limited to 128 characters. So this conversion can be improved
        // a bit to ensure valid index names for model names.
        : modelDefinition.model.definition.name.toLowerCase();
}

function liftData(modelDefinition, data, defaultAction, markedAsRemoved) {
    if (!Array.isArray(data)) {
        data = [data];
    }

    const indexingProperties = ['@search.action'];

    // Convert all data values.
    return data.map(x => {
        const object = {};

        for (const viewProperty in x) {
            const dbProperty = translateDBPropertyFromView(modelDefinition, viewProperty, indexingProperties);

            // Allow special indexing properties to pass.
            if (indexingProperties.indexOf(dbProperty) >= 0) {
                object[dbProperty] = x[viewProperty];
            }

            // Otherwise, handle model properties.
            else {
                const property = modelDefinition.properties[viewProperty];

                object[dbProperty] = convertValue(property, x[viewProperty]);

                // Make sure that dates are always ISO dates.
                if (property.type === Date && object[dbProperty] !== null) {
                    object[dbProperty] = object[dbProperty].toISOString();
                }
            }
        }

        // Set delete action if the document is marked as removed.
        if (markedAsRemoved !== undefined && markedAsRemoved.indexOf(x) >= 0) {
            object[indexingProperties[0]] = 'delete';
        }

        // Otherwise, put Azure Search action alongside with data.
        // We assume that the data contains this if the default action is not defined.
        else if (defaultAction !== undefined) {
            object[indexingProperties[0]] = defaultAction;
        }

        return object;
    });
}

function liftViewProperties(modelDefinition, object) {
    const viewObject = {};

    for (const dbProperty in object) {
        try {
            const viewProperty = translateViewPropertyFromDB(modelDefinition, dbProperty);

            viewObject[viewProperty] = object[dbProperty];
        } catch (error) {
            // Property is dropped because it cannot be translated to view property.
        }
    }

    return viewObject;
}

function translateDBPropertyFromView(modelDefinition, viewProperty, variables) {

    // Local variables are resolved first if available.
    if (variables !== undefined && variables !== null && variables.indexOf(viewProperty) >= 0) {
        return viewProperty;
    }

    // Then try to resolve the property name from special model properties unless it
    // is a non-allowed map property.
    if (modelDefinition.properties[viewProperty] !== undefined &&
        modelDefinition.properties[viewProperty].azureSearch !== undefined &&
        modelDefinition.properties[viewProperty].azureSearch.propertyName !== undefined &&
        !isMapProperty(modelDefinition.properties[viewProperty]))
    {
        return modelDefinition.properties[viewProperty].azureSearch.propertyName;
    }

    // Lastly, resolve property name directly from model properties unless it is a map property.
    if (modelDefinition.properties[viewProperty] !== undefined &&
        !isMapProperty(modelDefinition.properties[viewProperty]))
    {
        return viewProperty;
    }

    throw new Error(`'${viewProperty}' is not any of available model properties for '${modelDefinition.model.definition.name}': ${properties(modelDefinition).join(', ')}, or it doesn't have a valid 'azureSearch.propertyName' configuration.`);
}

function translateViewPropertyFromDB(modelDefinition, dbProperty) {
    for (const property in modelDefinition.properties) {

        // Resolve custom property name.
        if (modelDefinition.properties[property].azureSearch !== undefined &&
            modelDefinition.properties[property].azureSearch.propertyName === dbProperty)
        {
            return property;
        }
    }

    // Resolve view property == db property unless it is a map property.
    if (modelDefinition.properties[dbProperty] !== undefined &&
        !isMapProperty(modelDefinition.properties[dbProperty]))
    {
        return dbProperty;
    }

    throw new Error(`'${dbProperty}' is not any of available model properties: ${properties(modelDefinition).join(', ')}, or it doesn't have a valid 'azureSearch.propertyName' configuration.`);
}

function apiVersion() {
    return '2016-09-01';
}

function apiUrl(parts) {
    return parts.map(x => x.replace(/\/$/, '')).join('/');
}

function apiPost(url, apiKey, allowUnsafeKeys) {
    const query = { 'api-version': apiVersion() };

    // Set flag to allow unsafe keys.
    if (allowUnsafeKeys) {
        query.allowUnsafeKeys = 1;
    }

    return request
        .post(apiUrl(url))
        .query(query)
        .set('api-key', apiKey);
}

function apiPut(url, apiKey) {
    return request
        .put(apiUrl(url))
        .query({ 'api-version': apiVersion() })
        .set('api-key', apiKey);
}

function apiDelete(url, apiKey) {
    return request
        .del(apiUrl(url))
        .query({ 'api-version': apiVersion() })
        .set('api-key', apiKey);
}

function apiGet(url, apiKey) {
    return request
        .get(apiUrl(url))
        .query({ 'api-version': apiVersion() })
        .set('api-key', apiKey);
}

function convertValue(modelProperty, value) {
    if (value === undefined || value === null) {
        return value; // Support undefined & null semantics.
    }

    if (modelProperty.type === String) {
        return value.toString();
    }

    if (modelProperty.type === Array && Array.isArray(value)) {
        return value;
    }

    if (modelProperty.type === Boolean) {
        return !!value;
    }

    if (modelProperty.type === Number) {
        if (modelProperty.azureSearch !== undefined && modelProperty.azureSearch !== null && !!modelProperty.azureSearch.integerProperty) {
            return parseInt(value, 10);
        }

        return parseFloat(value);
    }

    if (modelProperty.type === Date) {
        return new Date(value);
    }

    if (modelProperty.type === GeoPoint) {
        return new GeoPoint(value);
    }

    throw new Error(`Unknown or unsupported value '${value}'`);
}

function convertTypeTo(modelName, propertyName, type, specialSettings) {
    if (type === null || type === undefined) {
        throw new Error('Type cannot be null or undefined');
    }

    if (type === String) {
        return 'Edm.String';
    }

    if (type === Array) {
        return 'Collection(Edm.String)';
    }

    if (type === Boolean) {
        return 'Edm.Boolean';
    }

    if (type === Number && specialSettings.integerProperty === 64) {
        return 'Edm.Int64';
    }

    if (type === Number && (specialSettings.integerProperty === 32 || specialSettings.integerProperty === true)) {
        return 'Edm.Int32';
    }

    if (type === Number) {
        return 'Edm.Double';
    }

    if (type === Date) {
        return 'Edm.DateTimeOffset';
    }

    if (type === GeoPoint) {
        return 'Edm.GeographyPoint';
    }

    throw new Error(`Unknown or unsupported type for property '${propertyName}' in '${modelName}'`);
}

function properties(modelDefinition) {
    return Object.keys(modelDefinition.properties || {})
        .filter(x => !isMapProperty(modelDefinition.properties[x]));
}

function isMapProperty(property) {
    return property !== undefined && property.azureSearch !== undefined &&
           ["@odata.count", "@search.score"].indexOf(property.azureSearch.propertyName || '') >= 0;
}

function isRetrievableProperty(property) {
    return property !== undefined && (property.azureSearch === undefined || property.azureSearch.retrievable !== false);
}

function isSearchableProperty(property) {
    return property !== undefined && (property.azureSearch === undefined || property.azureSearch.searchable !== false);
}
